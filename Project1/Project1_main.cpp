#include<iostream>
#include<string>
using namespace std;

#include <process.h>
#include <Windows.h>
#include <vector>

class ProcessLine {
private:
	int _sequence;
	string _name;
	string _params;

public:
	ProcessLine(int sequence,string name,string params = "") : _sequence(sequence), _name(name), _params(params) {}
	
	int getSequence() { return this->_sequence; }
	string getName() { return this->_name; }
	string getParams() { return this->_params; }

	string toString() {
		return std::to_string(this->getSequence()) + ", " + this->getName() + ", " + this->getParams();
	}

};

int main()
{
	//	1. Parse lines, while lines are readable
	//	2. Create ProcessLine object that takes in the params
	ProcessLine *process = new ProcessLine(4, "\"C:/windows/notepad\"", "df");
	ProcessLine *process2 = new ProcessLine(2, "\"C:/windows/notepad\"", "df");
	ProcessLine *process3 = new ProcessLine(1, "\"C:/windows/notepad\"", "df");
	ProcessLine *process4 = new ProcessLine(1, "\"C:/windows/notepad\"", "df");
	//cout << process->toString() << endl;

	//	3. Add the object to a vector that will hold all objects once file is finished reading
	vector<ProcessLine> procs;
	procs.push_back(*process);
	procs.push_back(*process2);
	procs.push_back(*process3);
	procs.push_back(*process4);

	for (std::vector<ProcessLine>::iterator it = procs.begin(); it != procs.end();++it)
	{
		cout << it->toString() << endl;
	}

	//	4. Sort the objects based on it's Launch Group number
	//	5. For every group of launch group, pass the objects to a function
	//	- This function will take the Name and params and create the process
	//	- It should return a string with[Launch Group, Kernel Time, User Time, Exit Code, Name, Params]
	//	- If the command fails, add the name and param to a array for all failed process
	//	6. Output all passed processes and then all failed process

}